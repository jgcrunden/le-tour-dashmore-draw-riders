.PHONY: clean test
CC=gcc
EXE=draw-riders
SRC=$(wildcard *.c)
OPTIMISE=-O2

$(EXE): $(SRC)
	@$(CC) $(OPTIMISE) $^ -o $@


clean:
	@rm -f $(EXE)

