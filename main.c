#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>

#define OPTSTR "f:n:c:t:"
#define DEFAULT_TEAM_SIZE 7
#define DEFAULT_CONTENDER_SIZE 10
#define DEFAULT_CONTENDERS_PER_TEAM 1

typedef struct node {
    char *name;
    struct node *next;
} rider;

typedef struct team_struct {
    char *teamName;
    char **riders;
} team;

extern char *optarg;

void addRider(rider **head, char *riderName) {
    rider *currentPtr = NULL;
    rider *previousPtr = NULL;
    if(*head == NULL) {
        *head = malloc(sizeof(rider));
        currentPtr = *head;
    } else {
        currentPtr = *head;
        previousPtr = currentPtr;
        currentPtr = currentPtr->next;
        
        while(currentPtr != NULL) {
            previousPtr = currentPtr;
            currentPtr = currentPtr->next;
        }
        currentPtr = malloc(sizeof(rider));
        previousPtr->next = currentPtr;
    }
    currentPtr->name = riderName;
    currentPtr->next = NULL;

}

char *getNRider(rider **head, int position) {
    rider *current = *head;
    char *result = NULL;


    if(position == 0) {
        *head = current->next;
        result = current->name;
        free(current);
        return result;
    } else {
        rider *previousRider = NULL;
        int count = 0;
        while(current != NULL && count < position) {
            previousRider = current;
            current = current->next;
            count++;
        }

        if (current == NULL) {
            printf("Rider not found at position %d. Not enough riders in the linked list\n", position);
        } else {
            result = current->name;
            previousRider->next = current->next;
            free(current);
        }
    }
    return result;
}

void deleteNRiders(rider **head, int n) {
    while(*head != NULL && n != 0) {
        rider *tmp = *head;
        *head = (*head)->next;
        free(tmp->name);
        free(tmp);
        n--;
    }
}

void printRiders(rider *currentPtr) {
    if(currentPtr == NULL) {
        printf("List is empty\n");
    } else {
        while(currentPtr != NULL) {
            printf("%s --> ", currentPtr->name);
            currentPtr = currentPtr->next;
        }
        printf("NULL\n");
    }
}

int getArrayLength(char *arr) {
    int counter = 1;
    while((*arr++) != '\0') {
        counter++;
    }
    return counter;
}

char *setRidersFile(char *fileName) {
    int len = getArrayLength(fileName);
    char *pf = (void *)malloc(len * sizeof(char));
    strcpy(pf, fileName);
    return pf;
}

int setContenderNumbers(char *contenders) {
    int a, b;
    sscanf(contenders, "%d:%d", &a, &b);
    return a;
}

int setcontendersPerTeam(char *contenders) {
    int a, b;
    sscanf(contenders, "%d:%d", &a, &b);
    return b;
}

char **getTeamNames(char **args, int index) {
    return args += --index;
}

void freeMemory(int anyNumber, ...) {
    va_list params;
    va_start(params, anyNumber);
    void *ptr = NULL;
    while((ptr = va_arg(params, void *)) != NULL) {
        free(ptr);
    }
    va_end(params);
    return;
}

int getNumTeams(char **teams) {
    int counter = 0;
    for(; *teams != NULL; teams++, counter++)
        ;

    return counter;
}

rider *getRidersFromFile(char *file, int *totalRiders) {
    FILE *fp = NULL;
    if ((fp = fopen(file, "r")) == NULL) {
        printf("No such file\n");
        return NULL;
    }

    ssize_t read;
    size_t bufferSize = 32;
    char *buffer = NULL;
    buffer = (void *)malloc(bufferSize * sizeof(char));

    rider *head = NULL;

    while((read = getline(&buffer, &bufferSize, fp)) > 0) {
        (*totalRiders)++;
        char *bufferTmp = NULL;
        bufferTmp = buffer;
        bufferTmp += read -1;
        *bufferTmp = '\0';
        addRider(&head, buffer);
        buffer = (void *)malloc(bufferSize * sizeof(char));
    }
    // Free unused mem after last iteration
    free(buffer);
    fclose(fp);
    return head;
}

void printTeams(team *teams[], int numTeams, int teamSize, int leaderCount) {
    printf("=====================\n");
    printf("\tTEAMS\n");
    printf("=====================\n");

    for(int i =0; i < numTeams; i++) {
        //sleep(1);
        printf("---------------------\n");
        printf("TEAM: %s\n", teams[i]->teamName);
        printf("---------------------\n");
        char **tmpRider = NULL;
        tmpRider = teams[i]->riders;
        for(int j = 0; j < teamSize; j++) {
            //sleep(0.5);
            if(j < leaderCount) {
                printf("Leader: %s\n", *tmpRider);
            } else {
                printf("\t%s\n", *tmpRider);
            }
            tmpRider++;
        }
        
    }
}

void saveDrawToFile(team *teams[], int numTeams, int teamSize) {
    FILE *fp = NULL;
    char fileName[] = "draw-results.txt";
    char *fileNamePtr = fileName;

    fp = fopen(fileNamePtr, "w");
    if (fp == NULL) {
        printf("Issue creating file\n");
        return;
    }
	// ashmores[0] = createTeam("The Buggly Wugglies", "#FFA500", [8]string{"POGAČAR Tadej","VINGEGAARD Jonas","SCHACHMANN Maximilian","MADOUAS Valentin","NEILANDS Krists","PETIT Adrien","PHILIPSEN Jasper","DUCHESNE Antoine"})
    for(int i =0; i < numTeams; i++) {
		fprintf(fp, "ashmores[%d] = createTeam(\"%s\", \"COLOUR\", [%d]string{", i, teams[i]->teamName, teamSize);
		//fprintf(fp, "{\n\t\"%s\",\n\t\"\",\n\t\"COLOUR\",\n\t\"\",\n\t0,\n\t\"\",\n\t0,\n\tmake([]string, 0, 4),\n\t[]rider{\n", teams[i]->teamName);
        char **tmpRider = NULL;
        tmpRider = teams[i]->riders;
        for(int j = 0; j < teamSize; j++) {
			fprintf(fp, "\"%s\", ", *tmpRider);
			//fprintf(fp, "\t\t{\n\t\t\t\"%s\",\n\t\t\t\"\",\n\t\t\t\"\",\n\t\t\t0,\n\t\t\t\"\",\n\t\t\t0,\n\t\t\t0,\n\t\t\t0,\n\t\t\t0,\n\t\t\t0,\n\t\t\t0,\n\t\t\t0,\n\t\t\tfalse,\n\t\t\tmake([]string, 0, 4),\n\t\t},\n", *tmpRider);
			
            tmpRider++;
        }
		fprintf(fp, "})\n");
		//fprintf(fp, "\t},\n},\n");
    }

    fclose(fp);
    return;
}

int main(int argc, char *argv[]) {
    time_t calendar_start = time(NULL);
    int opt;
    char *fileName;
    int teamSize = DEFAULT_TEAM_SIZE;
    int numContenders = DEFAULT_CONTENDER_SIZE;
    int contendersPerTeam = DEFAULT_CONTENDERS_PER_TEAM;
    int numTeams;
    char **teamNames;

    while((opt =  getopt(argc, argv, OPTSTR)) != -1) {
        switch(opt) {
            case 'f':
                fileName = setRidersFile(optarg);
                break;
            case 'n':
                teamSize = atoi(optarg);
                if(teamSize == 0) {
                    printf("Invalid team size: %d\n", teamSize);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'c':
                numContenders = setContenderNumbers(optarg);
                contendersPerTeam = setcontendersPerTeam(optarg);
                if (numContenders == 0 || contendersPerTeam == 0) {
                    printf("Contenders should be formatter as NUM:NUM where the first number the top n riders in the contender draw and the second number is the number of contenders per team\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 't':
                teamNames = getTeamNames(argv, optind);
                numTeams = getNumTeams(teamNames);
                break;
            case '?':
                printf("Invalid options\n");
                exit(EXIT_FAILURE);
        }
    }

    if((contendersPerTeam * numTeams) > numContenders) {
        printf("Please adjust the number of contenders for the draw. number of teams multiplied by number of contenders needs to be less than number of contenders in the draw\n");
        freeMemory(0, fileName);
        exit(EXIT_FAILURE);
    }

    int totalRiders = 0;
    rider *head = getRidersFromFile(fileName, &totalRiders);
    if(head == NULL) {
        printf("Problem accessing file and generating linked list of riders\n");
        exit(EXIT_FAILURE);
    }

    team *teams[numTeams];
    char **teamNamesTmp = teamNames;
    for(int i = 0; i < numTeams; i++, teamNamesTmp++) {
        teams[i] = malloc(sizeof(team));
        teams[i]->teamName = *teamNamesTmp;
        teams[i]->riders = malloc(teamSize * sizeof(char*));
    }
    
    time_t t;
    srand((unsigned) time(&t)); 
    // Pick favourite riders
    int riderPosition = 0;
    int remainingRidersPerTeam = teamSize;
    int contendersPerTeamTmp = contendersPerTeam;
    while(contendersPerTeamTmp > 0) {
        contendersPerTeamTmp--;
        remainingRidersPerTeam--;
        for(int i = 0; i < numTeams; i++, numContenders--, totalRiders--) {
            srand((unsigned) time(&t)); 
            int riderNum = rand() % numContenders;
            char **tmpRider = NULL;
            tmpRider = (teams[i]->riders);
            tmpRider += riderPosition;
            *tmpRider = getNRider(&head, riderNum);
        }
        riderPosition++;
    }

    deleteNRiders(&head, numContenders);
    totalRiders -= numContenders;
    while(remainingRidersPerTeam > 0 ) {
        remainingRidersPerTeam--;
        for(int i = 0; i < numTeams; i++, totalRiders--) {
            srand((unsigned) time(&t)); 
            int riderNum = rand() % totalRiders;
            char **tmpRider = NULL;
            tmpRider = (teams[i]->riders);
            tmpRider += riderPosition;
            *tmpRider = getNRider(&head, riderNum);
        }
        riderPosition++;

    }
    saveDrawToFile(teams, numTeams, teamSize);
    // sleep(2);
    printTeams(teams, numTeams, teamSize, contendersPerTeam);

    // free teams riders array and each individual rider
    for(int i = 0; i < numTeams; i++) {
        char **tmpRider = NULL;
        tmpRider = teams[i]->riders;
        for(int j = 0; j < teamSize; j++) {
            char **next = tmpRider +1;
            free(*tmpRider);
            tmpRider = next;
        }
        free(teams[i]->riders);
        free(teams[i]);
    }

    // Free linked list of riders
    rider *tmp = head;
    rider *nextPtr = NULL;
    while((tmp) != NULL) {
        nextPtr = tmp->next;
        free(tmp->name);
        free(tmp);
        tmp = nextPtr;
    }

    // Free filename
    free(fileName);
    time_t calendar_end = time(NULL);
    
    return 0;
}
